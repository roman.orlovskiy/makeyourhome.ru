import React from 'react';
import Layout from 'blocks/layout/layout';
import api from 'blocks/api/api';
import L10n from 'blocks/l10n/l10n';

import grid from 'blocks/grid/grid.css';
import text from 'blocks/text/text.css';
import Link from 'blocks/link/link';

class Order extends React.Component {
    constructor(p_, context) {
        super(p_, context);

        this.state = {
            text: '',
            phone: '',
            loading: false
        };
    }
    onChange(value, field) {
        this.setState({[field]: value})
    }
    send(user) {
        const self = this;
        const s_ = this.state;
        const p_ = this.props;

        self.setState({loading: true});
        api.post('projects/individualOrder', {user: user, text: s_.text, phone: s_.phone, projectId: p_.location.query.projectId})
            .then(() => {
                self.setState({
                    msg: L10n('project.changeSuccess'),
                    loading: false
                });
            });
    }
    onResponseAuth(response) {
        if (!response.error) {
            this.setState({user: response});
        }

        this.setState({loading: false});
    }
    onResponseRegistration() {
        this.setState({loading: false});
    }
    render() {
        const p_ = this.props;
        const s_ = this.state;

        return (
            <Layout
                loading={s_.loading}
                isPage={true}
            >
                <div>
                    <div className={`${grid.mbMini} ${text.colored}`}>
                        {s_.msg}
                    </div>
                    <div className={`${grid.mbMini} ${text.preWrap}`}>
                        {(() => {
                            if (p_.location.query.projectId) {
                                return L10n('project.changeText');
                            }
                            else {
                                return L10n('project.individualText');
                            }
                        })()}
                    </div>
                    <div className={`${grid.mbMini} ${grid.mtNormal} ${text.center}`}>
                      {L10n('ourMail')}{': '}
                      <Link className={`${text.colored}`} href={`mailto:${L10n('mail')}`} target="blank">
                        {L10n('mail')}
                      </Link>
                    </div>
                </div>
            </Layout>
        );
    }
}
Order.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default Order;
