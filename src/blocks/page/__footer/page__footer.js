import React from 'react';
import footer from './page__footer.css';
import grid from 'blocks/grid/grid.css';
import text from 'blocks/text/text.css';
import Link from 'blocks/link/link';
import item from 'blocks/item/item.css';
import L10n from 'blocks/l10n/l10n';

class PageFooter extends React.Component {
    constructor(p_) {
        super(p_);

        this.state = {};
    }
    render() {
        return (
            <div className={footer.wrapper}>
                <div className={footer.line}></div>
                <div className={footer.content}>
                    <div className={`${grid.w25} ${grid.w30_tab} ${item.none_tabMini} ${text.underline}`}>
                        <Link to="/posts/jvYxHlcL9jwtc3S">
                            {L10n('confidentiality')}
                        </Link>
                    </div>
                    <div className={`${text.center} ${grid.w100_tabMini} ${grid.mbMini_mob}`}>
                        <div className={`${text.colored} ${text.bold} ${text.mdPlus} ${text.normal_mob} ${grid.mbMicro}`}>
                            <Link to="/">
                                {L10n('phone')}
                            </Link>
                        </div>
                        <div className={`${text.normal} ${text.micro_mob}`}>
                            <Link href={`mailto:${L10n('mail')}`} target="blank">
                                {L10n('mail')}
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PageFooter;
